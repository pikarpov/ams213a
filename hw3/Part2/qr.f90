!The program performs QR decomposition via Householder method
!to solve the equation Ax=B where
!INPUTS:
!   A = m-by-n Vandermonde matrix
!   B = m size vector
!OUTPUTS:
!   X = m-by-n matrix solution
!   E = m-by-n matrix calculating the error (E=AX-B)
!
!COMPILE by typing: make
program qr_h

    !use globaldata
    use LinAl
    implicit none

    character*100 filename, name
    real, dimension(:,:), allocatable :: mat, A, diff, X, E, Q, R, I,B,P,fit_er
    integer, dimension(:), allocatable :: s
    integer :: msize, nsize, Am, An, Bm, Bn, Um, Un, j, poly, k
    logical :: singular

    !load matrix A and create a permanent copy As
    filename = 'atkinson.dat'! 'Amat.dat' !
    call readandallocatemat(mat,msize,nsize,filename)
    poly = 5    !fitting polynomial order
    Am = msize
    An = poly+1

    allocate(A(Am,An))
    allocate(I(Am,Am))
    allocate(B(Am,1))

    A(:,1)=1
    do j=1,poly
        A(:,j+1)=mat(:,1)**j
    enddo

    B(:,1)=mat(:,2)
    !Am = msize
    !An = nsize
    !A = mat
    name='A init'
    call printmat(A,Am,An, name)
    deallocate(mat)

    allocate(Q(Am,Am))
    allocate(R(Am,An))

    !perform QR decomposition
    call QR(A,Am,An,Q,R)
    allocate(diff(Am,An))
    diff = A - matmul(Q,R)
    name = 'A-QR'
    call printmat(diff,Am,An, name)
    call fnorm(diff,Am,An)
    deallocate(diff)

    I = 0
    forall(j=1:Am)I(j,j)=1
    allocate(diff(Am,Am))
    diff=matmul(transpose(Q),Q)-I
    name = 'Q^TQ-I'
    call printmat(diff,Am,Am, name)
    call fnorm(diff,Am,Am)

    allocate(X(An,1))
    allocate(P(Am,1))
    P=matmul(transpose(Q),B)
    call BackSub(R,P,X,Am,An,Am,1)
    name = 'X ='
    call printmat(X,An,1, name)

    !write fit coefficients to a file
    filename = 'fit.dat'
    open(11,file=filename)
    do j=1,An
        write(11,*) X(j,1)
    enddo
    close(11)


    !calculate resulting error by looking at the first n rows (the rest are all '0')
    allocate(E(An,1))
    !call Error(R,B,X,E,Am,An,Am,1)
    E = matmul(R(:An,:),X)-P
    name = 'E ='
    call printmat(E,An,1, name)
    call fnorm(E,An,1)


    allocate(fit_er(Am,1))
    do k=1,Am
        do j=1,An
            B(k,1)=B(k,1)-(X(j,1)*A(k,2)**(j-1))
        enddo
        fit_er(k,1) = B(k,1)
    enddo

    name='fit_er'
    call printmat(fit_er,Am,1, name)
    call fnorm(fit_er,Am,1)

endprogram qr_h
