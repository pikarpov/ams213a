'''
Plotting data and fits

-Platon Karpov
'''

from matplotlib import style
import matplotlib.pyplot as plt
from numpy import genfromtxt
import xlrd
import sys
import numpy as np
from Fitting import *
try:
    from Fitting import *
except ImportError:
    pass

def main():
    #size of the plot window
    plt.figure(figsize=(10,6), dpi=80)
    x, y = PlotData('Data')   #plots the data; go to the function to indicta filename

    PlotFunction('Fit')
    
    plt.legend(loc=4)
    plt.grid(True)

    #plt.xlim([0.0, 1.0])
    plt.tick_params(axis='x', labelsize=12)
    #plt.ylim([0.0, 6.0])
    plt.tick_params(axis='y', labelsize=12)
    plt.xlabel(r'x', fontsize=18)
    plt.ylabel(r'y', fontsize=18)
    plt.title('3th order fit')

    plt.savefig('3fit.png')


# Desired fit Function
def func(x,a,b,c,d):
    return a+b*x+c*x**2+d*x**3
	#return (0.57465866741953797+4.7258614421429037*x+
#								  -11.128217777645691*x**2+
#									  7.6686776229110292*x**3)


def PlotData(name):
    print("----------- Begin PlotData -----------")
    
    #path to your data
    filename = r'atkinson.dat'     
    
    #Sheet number in case it is .xlsx file;  won't have any effect for .txt and such
    sheetnumb = 0

    values = ImportData(filename, sheetnumb)

    x = values[0]     #Column containing x-values
    y = values[1]     #Column containing y-values

    plt.plot(x, y, marker = 'o', linestyle='-', label = name)

    print("----------- End PlotData ----------- \n")
    
    return x,y


def PlotFunction(name):
    x = np.linspace(0,1,100)
    y = np.zeros(len(x))

    filename = r'fit.dat'     
    coef = np.loadtxt(filename)

    print(coef)
    
    for j in range(len(y)):
        for i in range(len(coef)):
            y[j]=y[j]+coef[i]*x[j]**i
    
    plt.plot(x, y, label = name)
        
if __name__ == '__main__':
   main()
