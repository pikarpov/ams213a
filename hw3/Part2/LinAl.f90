module LinAl
implicit none
contains

!********************************************************

subroutine readandallocatemat(mat,msize,nsize,filename)
character*100 filename
real, dimension(:,:), allocatable, intent(in out) :: mat
integer :: msize, nsize, i,j

! Reads a file containing the matrix A
! Sample file:
! 3  4
! 1.2     1.3     -1.4    3.31
! 31.1    0.1     5.411   -1.23
! -5.4    7.42    10      -17.4
! Note that the first 2 lines are the matrix dimensions,
! then the next msize lines are the matrix entries
! Note that entries must be separated by a tab.
! Then allocates an array of size (msize,nsize), populates the matrix,
! and returns the array.

! This routine takes as INPUTS:
! filename = a character string with the name of file to read
! This routine returns as OUTPUTS:
! msize = first dimension of read matrix
! nsize = second dimension of read matrix
! mat = read matrix. Note that mat type is real.

open(10,file=filename)

! Read the matrix dimensions
read(10,*) msize,nsize

! Allocate matrix
allocate(mat(msize,nsize))

! Read matrix
do i=1,msize
   read(10,*) ( mat(i,j), j=1,nsize )
enddo

close(10)
end subroutine readandallocatemat


subroutine printmat(mat, msize, nsize, name)
    character*100 name
    integer :: msize,nsize,i,j
    real, dimension(msize,nsize) :: mat

    ! prints out the matrix in a human readable form

    ! This routine takes as INPUTS:
    ! msize = first dimension of read matrix
    ! nsize = second dimension of read matrix
    ! mat = matrix

    print*,'---------'
    print*, name
    do i=1,msize
        write(*,*)(mat(i,j), j=1,nsize)
    enddo
    print*,'---------'
endsubroutine printmat


subroutine trace(mat,msize,nsize)
    real :: tr=0.0
    integer :: msize,nsize, i
    real, dimension(msize,nsize) :: mat

    if (msize/=nsize) then
        print*, 'Not a square matrix, trace is undefined'
    else
        do i=1, msize
            tr = tr+mat(i,i)
        enddo
        print*, 'Trace =',tr
    endif
endsubroutine trace


subroutine fnorm(mat,msize,nsize)
    !calculates Frobenius Norm
    !INPUTS:
    !mat = matrix
    !msize = first dimension of the matrix
    !nsize = second dimension of the matrix

    real :: n
    integer :: msize,nsize,i,j
    real, dimension(msize,nsize) :: mat

    n=0.0
    do j=1, nsize
        do i=1,msize
            n = n+mat(i,j)**2
        enddo
    enddo
    n = sqrt(n)
    print*, 'Frobenius Norm of matrix = ',n
endsubroutine fnorm

subroutine enorm(column,msize,n)
    !calculates Eucledian Norm
    !INPUTS:
    !column = vector
    !msize = dimension of the vector
    !OUTPUS:
    !n = eucledian norm
    
    real :: n
    integer :: msize, i
    real, dimension(msize) :: column
    n=0.0
        do i=1,msize
            n = n+column(i)**2
        enddo
        n = sqrt(n)
endsubroutine enorm


subroutine QR(A,Am,An,Q,R)
    character*100 name
    integer :: Am, An,i,j,k
    real :: n,s
    real, dimension(Am) :: v
    real, dimension(Am,An) :: A,R
    real, dimension(Am,Am) :: Q,H

    ! performs QR decomposition via Householder method

    ! This routine takes as INPUTS:
    ! Am = first dimension of read matrix
    ! An = second dimension of read matrix
    ! A = matrix
    ! This routine returns as OUTPUTS:
    ! Q,R = matrix

    Q = 0
    forall(i=1:Am)Q(i,i)=1
    R=A
    do i=1,An

        H = 0
        forall(i=1:Am)H(i,i)=1

        call enorm(R(i:,i),Am+1-i,n)
        s = sign(n,R(i,i))

        v = R(:,i)

        do j=1,Am
            if (j<i) then
                v(j)=0
            elseif (j==i) then
                v(j)=v(j)+s
                exit
            endif
        enddo

        call enorm(v,Am,n)

        v(i:)=v(i:)/n

        do j=1,Am
            do k=1,Am
                H(k,j) = H(k,j)-2*v(k)*v(j)
            enddo
        enddo

        R = matmul(H,R)
        Q = matmul(Q,H)

    enddo

    name = 'R'
    call printmat(R,Am,An, name)
    name = 'Q'
    call printmat(Q,Am,Am, name)

endsubroutine QR


subroutine GaussElim(A,B,Am,An,Bm,Bn,singular)
    character*100 name
    integer :: Am, An, Bm, Bn
    real, dimension(Am,An) :: A
    real, dimension(Bm,Bn) :: B
    logical :: singular
    integer :: p, pivot, i, j, k
    real :: frac

    ! performs Gaussian Elimination with partial pivoting

    ! This routine takes as INPUTS:
    ! Am, Bm = first dimension of read matrix
    ! An, Bn = second dimension of read matrix
    ! A, B = matrix
    ! singular = flag
    ! This routine returns as OUTPUTS:
    ! A, B = matrix

    do j=1,An-1

        !pivoting
        pivot=j
        do p=j,An
            if (abs(A(p,j))>abs(A(pivot,j))) then
                pivot=p
            endif
        enddo
        if (pivot /= j) then
            A([pivot,j],:) = A([j,pivot],:)
            B([pivot,j],:) = B([j,pivot],:)
        endif

        !check if diagonal term is zero
        if (A(j,j)==0) then
            singular = .True.
            print*,'Stop, A is singular'
            stop
        endif

        !gaussian elimination for both A and B
        do i=j+1,Am
            frac = A(i,j)/A(j,j)
            do k=j+1,An
                A(i,k)=A(i,k)-A(j,k)*frac
            enddo

            do k=1,Bn
                B(i,k)=B(i,k)-B(j,k)*frac
            enddo

            !'eliminate' lower triangular terms
            A(i,j) = 0.0
        enddo
    enddo
endsubroutine GaussElim


subroutine BackSub(R,B,X,Am,An,Bm,Bn)
    integer :: Am, An, Bm, Bn, i,j,k
    real, dimension(Am,An) :: R
    real, dimension(Bm,Bn) :: B
    real, dimension(An,Bn) :: X
    real :: sigma

    !substitudes back to find X

    ! This routine takes as INPUTS:
    ! Um, Bm = first dimension of read matrix
    ! Un, Bn = second dimension of read matrix
    ! U, B = matrix
    ! This routine returns as OUTPUTS:
    ! X = matrix

    print*, 'sizes', Am,An,Bm,Bn
    do i=An,1,-1
        do j=1,Bn
            sigma = 0.
            do k=i+1, An
                sigma=sigma+R(i,k)*X(k,j)
            enddo
            X(i,j)=(B(i,j)-sigma)/R(i,i)
        enddo
    enddo

endsubroutine BackSub


subroutine LU(A,Am,s,singular)
    character*100 name
    integer :: Am, i,j,k
    real, dimension(Am,Am) :: A
    integer, dimension(Am) :: s
    !real, dimension(Bm,Bn) :: B
    logical :: singular
    integer :: p, pivot
    real :: frac

    ! performs LU decomposition with partial pivoting

    ! This routine takes as INPUTS:
    ! Am = first & second dimensions of read matrix
    ! A = matrix
    ! This routine returns as OUTPUTS:
    ! A = matrix
    ! s = vector
    ! singular = flag

    !initialize s vector
    do j=1,Am
        s(j)=j
    enddo

    do j=1,Am

        !pivoting
        pivot=j
        do p=j,Am
            if (abs(A(p,j))>abs(A(pivot,j))) then
                pivot=p
            endif
        enddo
        if (pivot /= j) then
            A([pivot,j],:) = A([j,pivot],:)
            s([pivot,j]) = s([j,pivot])
        endif

        !check if diagonal term is zero
        if (A(j,j)==0) then
            singular = .True.
            print*,'Stop, A is singular'
            stop
        endif

        !gaussian elimination for both A and B
        do i=j+1,Am
            A(i,j) = A(i,j)/A(j,j)
            do k=j+1,Am
                A(i,k)=A(i,k)-A(i,j)*A(j,k)
            enddo

        enddo
    enddo
endsubroutine LU


subroutine LU_BackSub(A,Am,B,Bn,s,X)
    integer :: Am, Bn, i,j,k
    real, dimension(Am,Am) :: A
    real, dimension(Am,Bn) :: B,X,y
    integer, dimension(Am) :: s
    real :: sigma

    !substitudes back to find X

    ! This routine takes as INPUTS:
    ! Am = first and second dimension of read A matrix
    ! Bn = second dimension of read matrix B
    ! A, B = matrix
    ! s = vector
    ! This routine returns as OUTPUTS:
    ! X = matrix

    !re-order B based on s into matrix y
    do j=1,Am
        y(j,:)=B(s(j),:)
    enddo

    do k=1,Bn
        do j=1,Am-1
            do i=j+1,Am
              y(i,k)=y(i,k)-y(j,k)*A(i,j)
            enddo
        enddo
    enddo

    do i=Am,1,-1
        if (A(i,i)==0) then
            stop
        endif
        do j=1,Bn
        sigma=0.
        do k=i+1,Am
          sigma=sigma+A(i,k)*X(k,j)
        enddo
        X(i,j)=(y(i,j)-sigma)/A(i,i)
        enddo
    enddo

endsubroutine LU_BackSub


subroutine Error(A,B,X,E,Am,An,Bm,Bn)
    integer :: Am, An, Bm, Bn, i,j,k
    real, dimension(Am,An) :: A
    real, dimension(Bm,Bn) :: B, E
    real, dimension(Am,Bn) :: X
    real :: sigma

    !finds error of our calculation, E=AX-B

    ! This routine takes as INPUTS:
    ! Am, Bm = first dimension of read matrix
    ! An, Bn = second dimension of read matrix
    ! A, B, X = matrix
    ! This routine returns as OUTPUTS:
    ! E = matrix

    do k=1,Bn
        do j=1,An
            sigma=0.
            do i=1,An
                sigma=sigma+A(j,i)*X(i,k)
            enddo
            E(j,k)=sigma-B(j,k)
        enddo
    enddo
endsubroutine Error

end module LinAl
