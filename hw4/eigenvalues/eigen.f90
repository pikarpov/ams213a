! The program calculates eigenvalues of a matrix
!via QR method without a shift and with a shift
!INPUTS:
!   A = m-by-m symmetric matrix
!OUTPUTS:
!   lambda = m-size vector
!COMPILE by typing: make
program eigenvalues

    use LinAl
    implicit none

    character*100 filename, name
    real, dimension(:,:), allocatable :: mat, A, diff, X, E, Q, R, I,B,P,fit_er, V, A_old
    real, dimension(:), allocatable :: lambda
    integer :: msize, nsize, Am, An, j, k, count
    real :: mu
    real :: err=1.0
    logical :: singular

    !load matrix A
    filename = 'Amat.dat'
    call readandallocatemat(mat,msize,nsize,filename)
    Am = msize
    An = nsize
    allocate(A(Am,Am))
    A = mat
    name='A init'
    call printmat(A,Am,Am, name)

    allocate(lambda(Am))
    allocate(Q(Am,Am))
    allocate(R(Am,An))
    allocate(I(Am,Am))
    allocate(A_old(Am,Am))

    !Find eigenvalues with QR without shift
    count = 0
    do while (err>1e-5)
        A_old = A
        call QR(A,Am,An,Q,R)
        A = matmul(R,Q)
        A_old = abs(A)-abs(A_old)
        call fnorm(A_old,Am,Am, err)    !frobenius norm
        count = count+1
    enddo

    do j=1,Am
        lambda(j) = A(j,j)
    enddo

    print*, 'Eigenvalues (QR without shift)', lambda
    print*, 'converged in ',count,'iterations'


    !Find eigenvalues with QR with shift
    A = mat
    count = 0
    err = 1.0
    I = 0
    forall(j=1:Am)I(j,j)=1
    do while (err>1e-5)
        mu = A(Am,Am)   !shift
        A_old = A
        call QR(A-mu*I,Am,An,Q,R)
        A = matmul(R,Q)+mu*I
        A_old = abs(A)-abs(A_old)
        call fnorm(A_old,Am,Am, err)    !frobenius norm
        count = count+1
    enddo

    do j=1,Am
        lambda(j) = A(j,j)
    enddo

    print*, 'Eigenvalues (QR with shift)', lambda
    print*, 'converged in ',count,'iterations'

endprogram eigenvalues
