! The program calculates eigenvectors using inverse iteration method
!INPUTS:
!   A = m-by-m symmetric matrix
!OUTPUTS:
!   y = m-size eigenvector
!COMPILE by typing: make
program eigenvectors

    use LinAl
    implicit none

    character*100 filename, name
    real, dimension(:,:), allocatable :: mat,A,I,B
    real, dimension(:), allocatable :: lambda, y, x
    integer, dimension(:), allocatable :: s
    integer :: msize, nsize, Am, An, j, k
    real :: mu, n, err
    logical :: singular

    !load matrix A
    filename = 'Amat.dat'
    call readandallocatemat(mat,msize,nsize,filename)
    Am = msize
    An = nsize
    allocate(A(Am,Am))
    A = mat
    name='A init'
    call printmat(A,Am,Am, name)

    allocate(lambda(3))
    allocate(y(Am))
    allocate(x(Am))
    allocate(I(Am,Am))
    allocate(s(Am))
    allocate(B(Am,Am))

    !exact eigenvalues of A
    lambda = (/-8.0286, 7.9329, 5.6689/)

    I = 0
    forall(j=1:Am)I(j,j)=1

    !iterate to find all of the eigenvectors
    do j=1, size(lambda)
        !'guess' mu, which is close, but not exact to eigenvalue
        mu = lambda(j)+1e-2
        A = mat-mu*I

        !perform LU decomposition to find inverse B=(A-muI)^-1
        singular = .False.
        call LU(A,Am,s,singular)
        call LU_BackSub(A,Am,I,Am,s,B)

        !set vector x, with \\x\\^2=1
        x(1)=1
        forall(k=2:Am)x(k)=0

        !perform inverse iteration until convergence (to 1e-15)
        err = 1.0
        do while (err>1e-15)
            call enorm(matmul(B,x),Am,n)
            y = matmul(B,x)/n
            call enorm(abs(y)-abs(x),Am,err)
            x=y
        enddo

        print*, 'eigenvector',j,':',y/minval(abs(y))
    enddo
endprogram eigenvectors
