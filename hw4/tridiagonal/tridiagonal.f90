! The program tridiagonalizes a symnmetric square matrix via Householder method
!INPUTS:
!   A = m-by-m symmetric matrix
!OUTPUTS:
!   A = m-by-m tridiagonal symmetric matrix
!COMPILE by typing: make
program tridiagonal

    use LinAl
    implicit none

    character*100 filename, name
    real, dimension(:,:), allocatable :: mat, A
    integer, dimension(:), allocatable :: s
    integer :: msize, nsize, Am
    logical :: singular

    !load matrix A
    filename = 'Amat.dat'
    call readandallocatemat(mat,msize,nsize,filename)
    Am = msize
    allocate(A(Am,Am))
    A = mat
    name='A init'
    call printmat(A,Am,Am, name)

    !perform tridiagonalization
    call tridiag(A,Am)

    !print the result matrix
    name = 'tridiagonal A'
    call printmat(A,Am,Am, name)

endprogram tridiagonal
