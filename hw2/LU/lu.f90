!The program performs LU decomposition with partial pivoting
!to solve the equation AX=B where
!INPUTS:
!   A = m-by-m matrix
!   B = m-by-n matrix
!OUTPUTS:
!   X = m-by-n matrix solution
!   E = m-by-n matrix calculating the error (E=AX-B)
!
!COMPILE by typing: make
program gaussian

    !use globaldata
    use LinAl
    implicit none

    character*100 filename, name
    real, dimension(:,:), allocatable :: mat, A, B, As, Bs, U, X, E
    integer, dimension(:), allocatable :: s
    integer :: msize, nsize, Am, An, Bm, Bn, Um, Un
    logical :: singular

    !load matrix A and create a permanent copy As
    filename = 'Amat.dat'
    call readandallocatemat(mat,msize,nsize,filename)
    Am = msize
    An = nsize
    allocate(A(Am,An))
    allocate(As(Am,An))
    A = mat
    As = mat
    name='A init'
    call printmat(A,Am,An, name)
    deallocate(mat)

    !load matrix B and create a permanent copy Bs
    filename = 'Bmat.dat'
    call readandallocatemat(mat,msize,nsize,filename)
    Bm = msize
    Bn = nsize
    allocate(B(Bm,Bn))
    allocate(Bs(Bm,Bn))
    B = mat
    Bs = mat
    name='B init'
    call printmat(B,Bm,Bn, name)

    !call trace(mat, msize, nsize)

    !perform Gaussian Elimination
    singular = .False.
    allocate(s(Am))
    call LU(A,Am,s,singular)

    !print the result A and B matricies
    name='A post GaussElim'
    call printmat(A,Am,An, name)
    name='B post GaussElim'
    call printmat(B,Bm,Bn, name)

    !allocate an upper triangular matrix U and
    !perform back substitution to find X

    allocate(X(Bm,Bn))
    allocate(E(Bm,Bn))
    call LU_BackSub(A,Am,B,Bn,s,X)

    !find the error
    call Error(As,Bs,X,E,Am,An,Bm,Bn)

    !print results
    name='X'
    call printmat(X,Bm,Bn, name)
    name='Error'
    call printmat(E,Bm,Bn, name)

    !find eucledian norm of E
    call enorm(E,Bm,Bn)

endprogram gaussian
